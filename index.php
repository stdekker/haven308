<?php
/**
 * The main template file.
 */

	get_header(); ?>
	
	<section id="primary" class="masonry">
		<?php get_template_part( 'loop', 'news' ); ?>
	</section>
	
	<?php 
	get_sidebar();
	get_footer(); 

	?>