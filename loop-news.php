<?php

/* Custom news-page loop */
if (  $wp_query->max_num_pages > 1 && $paged) { haven_page_nav('nav-above'); }

		$count = 0;	
		$format = 'full';
		while(have_posts()) : 		
		$count++;	
		if($count >= 3) { 
			$format = 'short'; // shorten 
		}	
		html_article(the_post(), $format);					
		endwhile; 

		if (  $wp_query->max_num_pages > 1 ) { haven_page_nav('nav-below'); } ?>

<?php

// How to render a post as a 'news article '

function html_article(&$post, $format='full') { ?>
	<article <?php post_class('news-'.$format); ?>>
		<header>
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'boilerplate' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		</header>

		<section class="entry-meta"><time><?php the_time("j F `y"); ?></time> &rsaquo; <span class="author"><?php the_author(); ?></span></section>	
		<?php if(has_post_thumbnail()) : ?>
						<figure class="featured">
						<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('featured-'.$format); ?>
						</a></figure>
					<?php endif; ?>
		
		<section class="entry-content">
		<?php switch ($format) {
				case 'full'  :	
					the_content('&raquo; Lees meer &hellip;'); 
				break;
				
				case 'short' :				
					the_excerpt(); 
				break;
				}
		?>
		</section>

	</article>
<?php } // function news_article ?>

