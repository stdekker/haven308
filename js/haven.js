/*
	Any site-specific scripts you might have.
*/

WebFontConfig = {
			google: { families: [ 'Arvo', 'Bevan' ] }
		  };
		  (function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		  })();

$(document).ready(function(){

	$('#primary.masonry').imagesLoaded(function() {

		$(this).masonry({
			columnWidth : function( containerWidth ) {
					return containerWidth / 2;
				  }
		
		
		});

	});
	
	
	$(window).resize(function() {
	$("#primary.masonry").masonry();
		});


$('#access .menu-header li').hover(
			function() {
				console.log('in');
					$(this).children('ul').stop(true, true).show();
				}, 
			function() { 
				console.log('uit');
					$(this).children('ul').stop(true, true).hide();				
				});

});


