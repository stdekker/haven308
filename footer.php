<?php
/**
 * The template for displaying the footer.
 */
?>
		</div <!-- #positioning -->
	</div> <!-- #filler	-->
	
	<footer role="contentinfo" id="site-footer">
			
			<div class="copyright">
				<a href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="site-name">Scouting Jan van Hoof Groep <span class="rights">&copy; 1945 - <?php print(date('Y')); ?></span></a>
				<address>Nieuwe Haven 308, 2801 EG - Gouda</address>
			</div>	
	</footer><!-- footer -->
	
<?php wp_footer(); ?>
	</body>
</html>