<?php
/**
 * The Header for our theme.
 */
 
?><!DOCTYPE html>
<!--[if lt IE 7 ]><html dir="ltr" lang="nl" class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html dir="ltr" lang="nl" class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="nl" class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="nl" class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html dir="ltr" lang="nl" class="no-js"><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<title><?php wp_title(''); ?></title>
		<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php print($style_dir); ?>/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
		<?php $style_dir = get_bloginfo( 'stylesheet_directory' ); ?>	
		<script src="<?php print($style_dir); ?>/js/respond.min.js"></script>
		<script src="<?php print($style_dir); ?>/js/plugins.js"></script>
		<script src="<?php print($style_dir); ?>/js/haven.js"></script>
		
		<script type="text/javascript">
		
		WebFontConfig = {
			google: { families: [ 'Arvo', 'Bevan' ] }
		  };
		  (function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		  })();
		
		</script>
		
		<?php $style_dir = get_bloginfo( 'stylesheet_directory' ); ?>
		<script type="text/javascript" src="<?php print $style_dir;?>/js/modernizr.js"></script>
<?php
	
		if ( is_singular() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
			}
			
		wp_head();
?>
	</head>
	<body <?php 
	$bid = get_current_blog_id();
	body_class('blogid-'.$bid); ?>>
	
	<header role="banner" id="site-header">
		<section id="branding">
			<figure id="site-logo">
			<a href="<?php echo home_url( '/' ); ?>" title="Terug naar de voorpagina" rel="home">
			</a></figure>
			<h1 class="site-name"></h1>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		</section>
			
		<section id="access" role="navigation">
			<?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
			<a id="skip" href="#content" title="Snel naar de inhoud">Snel naar de inhoud</a>
			<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
		</section>
		
	</header>
	
	<div id="filler">
		<div id="positioning">