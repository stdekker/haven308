<?php
/**
 * The template for displaying Comments.
 */
?>

<?php if ( post_password_required() ) : ?>
				<p><?php echo( 'Dit bericht is beschermd met een wachtwoord.'); ?></p>
<?php
		return;
	endif;
?>

<?php
	// You can start editing here -- including this comment!
?>

<?php if ( have_comments() ) : ?>
<section id="comments">
			<!-- STARKERS NOTE: The following h3 id is left intact so that comments can be referenced on the page -->
			<h3 id="comments-title"><?php
				printf( _n( 'Een reactie op %2$s', '%1$s reacties op %2$s', get_comments_number(), 'boilerplate' ),
				number_format_i18n( get_comments_number() ), '' . get_the_title() . '' );
			?></h3>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
				<?php previous_comments_link( __( '&larr; Older Comments', 'boilerplate' ) ); ?>
				<?php next_comments_link( __( 'Newer Comments &rarr;', 'boilerplate' ) ); ?>
<?php endif; // check for comment navigation ?>

			<ol>
				<?php wp_list_comments( array( 'callback' => 'haven_comment' ) );	?>
			</ol>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
				<?php previous_comments_link( __( '&larr; Older Comments', 'boilerplate' ) ); ?>
				<?php next_comments_link( __( 'Newer Comments &rarr;', 'boilerplate' ) ); ?>
<?php endif; // check for comment navigation ?>

<?php else : // or, if we don't have comments:

	/* If there are no comments and comments are closed,
	 * let's leave a little note, shall we?
	 */
	if ( ! comments_open() ) :
?>
<?php endif; // end ! comments_open() ?>
<?php endif; // end have_comments() ?>
</comments>

<?php comment_form(); ?>