== Haven 308 =====
Custom wordpress theme for janvanhoof.nl

Contributors: Stijn Dekker

== Foundation =====

# HTML5 Boilerplate
https://github.com/h5bp/html5-boilerplate
Contributors: Nicolas Gallagher, Hans Christian Reinl, C�t�lin Maris, Mathias Bynens, Paul Irish, and Divya Manian.

# Boilerplate theme 
http://wordpress.org/extend/themes/boilerplate
Author: Aaron T. Grogg 


#jQuery Masonry
http://masonry.desandro.com/
Author: David DeSandro
