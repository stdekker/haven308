<?php
/**
 * The template for displaying Category Archive pages.
 */

get_header(); ?>
<section id="primary">

				<h1 class="archive-title"><?php
					printf( __( '%s archief', 'boilerplate' ), '' . single_cat_title( '', false ) . '' );
				?></h1>
	<section id="archive" class="masonry">
					<?php
						$category_description = category_description();
						if ( ! empty( $category_description ) )
							echo '' . $category_description . '';

					/* Run the loop for the category page to output the posts. */
					
					get_template_part( 'loop', 'category' );
					?>
	</section>				
</section>
<?php get_footer(); ?>